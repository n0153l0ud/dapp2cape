import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


@XmlRootElement(name = "Persona")
@XmlAccessorType(XmlAccessType.NONE)
public class Persona {
    @XmlElement(name = "nombre")
    public String nombre;
    @XmlElement(name = "edad")
    public int edad;
    @XmlElement(name = "genero")
    public String genero;

    @XmlElement(name = "estatus")
    public Boolean estatus;

    public Persona() {
    }

    public Persona(String nombre, int edad, String genero, Boolean estatus) {
        this.nombre = nombre;
        this.edad = edad;
        this.genero = genero;
        this.estatus = estatus;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public String getGenero() {
        return genero;
    }

    public Boolean getEstatus() {
        return estatus;
    }

    @Override
    public String toString() {
        return  "nombre: " + nombre + '\'' +
                ", edad: " + edad +
                ", genero: '" + genero + '\'' +
                ", estatus: " + estatus;
    }

    public static void serializacionXML() {
        try {
            XmlMapper xmlMapper = new XmlMapper();

            // serialize our Object into XML string
            String xmlString = xmlMapper.writeValueAsString(new Persona("Emilio", 20, "Masculino", true));

            // write to the console
            System.out.println(xmlString);

            // write XML string to file
            File xmlOutput = new File("serializacion.xml");
            FileWriter fileWriter = new FileWriter(xmlOutput);
            fileWriter.write(xmlString);
            fileWriter.close();
        } catch (JsonProcessingException e) {
            // handle exception
        } catch (IOException e) {
            // handle exception
        }
    }

   public static void deserealizacionXml(){
       try {
           JAXBContext context = JAXBContext.newInstance( Persona.class );
           Unmarshaller unmarshaller = context.createUnmarshaller();
           Persona persona = (Persona) unmarshaller.unmarshal(
                   new File("E:\\Practica06\\deserealizacion.xml") );

           System.out.println("Nombre: " +  persona.getNombre());
           System.out.println("Edad:" + persona.getEdad());
           System.out.println("Genero:" + persona.getGenero());
           System.out.println("Estatus:" + persona.getEstatus());

       } catch (JAXBException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }
   }

   public static void  serializcacionJSON(){
       Persona persona = new Persona("Angel", 22, "Masculino", true);

       Gson gson = new Gson();

       String jsonString = gson.toJson(persona);

       System.out.println(jsonString);
   }

   public static void deserializacionJSON(){
       String jsonString = "{'nombre':Andrea, 'edad':'27', 'genero':'Femenino', 'estatus':'true'}";

       Gson gson = new Gson();

       Persona persona = gson.fromJson(jsonString, Persona.class);

       System.out.println(persona.toString());
   }


    public static void main(String[] args) {
        System.out.println("Serializando a XML...");
        serializacionXML();
        System.out.println("Deserializando XML...");
        deserealizacionXml();
        System.out.println("Serializando a JSON...");
        serializcacionJSON();
        System.out.println("Deserializando a JSON...");
        deserializacionJSON();
    }
}


import modelo.Persona;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Socket miCliente = null;
        String host = "localhost";
        int puerto = 8000;
        try {
            System.out.println("Conectando al servidor");

            //Conexion al servidor mediante Socket TCP
            miCliente = new Socket(host, puerto);
            System.out.println("Me conecte al servidor");

            InputStreamReader streamSocket = new InputStreamReader(miCliente.getInputStream());

            BufferedReader lectorSocket = new BufferedReader(streamSocket);

            PrintWriter escritorSocket = new PrintWriter(miCliente.getOutputStream(),true);

            String mensajeTransmitido, mensajeRecibido;

            mensajeTransmitido = "Dame la lista de personas";

            System.out.println("Enviando mensaje");

            escritorSocket.println(mensajeTransmitido);

            System.out.println("Recibiendo mensaje");

            Gson gson = new Gson();

            mensajeRecibido = lectorSocket.readLine();

            Type collectionType = new TypeToken<ArrayList<Persona>>(){}.getType();
            ArrayList<Persona> personas = gson.fromJson(mensajeRecibido, collectionType);


            for (Persona persona : personas) {
                System.out.println(persona.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

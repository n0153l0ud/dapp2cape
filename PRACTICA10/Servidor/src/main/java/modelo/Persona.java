package modelo;

public class Persona {

    private Integer id;
    private String nombre;
    private String paterno;
    private String materno;
    private Integer edad;
    private String genero;

    public Persona() {
    }

    public Persona(Integer id, String nombre, String paterno, String materno, Integer edad, String genero) {
        this.id = id;
        this.nombre = nombre;
        this.paterno = paterno;
        this.materno = materno;
        this.edad = edad;
        this.genero = genero;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
    public  String toJson() {

        String cadenaJson="{";

        cadenaJson+="\"id\":\""+id + "\",";
        cadenaJson+="\"nombre\":\""+nombre + "\",";
        cadenaJson+="\"paterno\":\""+paterno + "\",";
        cadenaJson+="\"materno\":\""+materno + "\",";
        cadenaJson+="\"edad\":\""+edad + "\",";
        cadenaJson+="\"genero\":\""+genero + "\",";

        cadenaJson+="}";

        return cadenaJson;
    }


    public static String toJsonList(ArrayList<Persona> personas){
        String cadenaJson = "{\"personas\":[";

        for (Persona persona:personas) {
            cadenaJson += persona.toJson() + ",";
        }
        cadenaJson += "]}";
        return cadenaJson;
    }**/
}

import crud.CRUD;
import conexion.Conexion;
import modelo.Persona;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        int puerto = 8000;
         try {
            ServerSocket miServidor = new ServerSocket(puerto);
            Socket miCliente = null;

            System.out.println("Esperando conexion");

            miCliente = miServidor.accept();

            System.out.println("Conexion aceptada");


            InputStreamReader streamSocket = new InputStreamReader(miCliente.getInputStream());

            BufferedReader lectorSocket = new BufferedReader(streamSocket);

            PrintWriter escritorSocket = new PrintWriter(miCliente.getOutputStream(), true);

            String mensajeRecibido, mensajeEnviado;

            System.out.println("Esperando mensaje");

            mensajeRecibido = lectorSocket.readLine();

            System.out.println(mensajeRecibido);

            System.out.println("Mandando información");
            Conexion conexion = new Conexion();
            Connection conn = conexion.conecta();
            CRUD crud = new CRUD();

            ArrayList<Persona> personas;
            personas = crud.select(conn);

            Gson gson = new Gson();
            mensajeEnviado = gson.toJson(personas, ArrayList.class);

            escritorSocket.println(mensajeEnviado);

            System.out.println(mensajeEnviado);

        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }
}
package crud;

import modelo.Persona;

import java.sql.*;
import java.util.ArrayList;

public class CRUD {

    public ArrayList<Persona> select (Connection conn) throws SQLException {
        ArrayList<Persona> personas = new ArrayList<>();
        Statement myStmt = conn.createStatement();
        ResultSet myRs = myStmt.executeQuery("select id_persona,nombre, paterno, materno, edad, genero from persona");

        while (myRs.next()){
            Persona persona = new Persona();
            persona.setId(myRs.getInt(1));
            persona.setNombre(myRs.getString(2));
            persona.setPaterno(myRs.getString(3));
            persona.setMaterno(myRs.getString(4));
            persona.setEdad(myRs.getInt(5));
            persona.setGenero(myRs.getString(6));
            personas.add(persona);
            persona = null;
        }
        myStmt.close();
        myRs.close();
        return personas;
    }

    /**
    public int insert(Connection conn, Persona persona) throws SQLException {
        String sql = "insert into persona(id_persona, nombre, paterno, materno, edad, genero)\n" +
                "     values(null,?,?,?,?,?)";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, persona.getNombre());
        pstmt.setString(2, persona.getPaterno());
        pstmt.setString(3, persona.getMaterno());
        pstmt.setString(4, String.valueOf(persona.getEdad()));
        pstmt.setString(5, persona.getGenero());
        int i = pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Se ha insertado el registro");
        return 0;
    }
    **/
}

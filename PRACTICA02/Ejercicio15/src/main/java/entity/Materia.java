package entity;

public class Materia {
    private Integer idMat;

    public Materia() {

    }

    public Materia(Integer idMat) {
        this.idMat = idMat;
    }

    public Integer getIdMat() {
        return idMat;
    }

    public void setIdMat(Integer idMat) {
        this.idMat = idMat;
    }
}

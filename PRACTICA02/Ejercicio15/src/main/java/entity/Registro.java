package entity;

public class Registro {
    private Integer idReg;
    private String CURP;
    private Integer calif;
    private Integer idMat;
    private Integer idAlm;

    public Registro(){

    }

    public Registro(Integer idReg, String CURP, Integer calif, Integer idMat, Integer idAlm) {
        this.idReg = idReg;
        this.CURP = CURP;
        this.calif = calif;
        this.idMat = idMat;
        this.idAlm = idAlm;
    }

    public Integer getIdReg() {
        return idReg;
    }

    public void setIdReg(Integer idReg) {
        this.idReg = idReg;
    }

    public String getCURP() {
        return CURP;
    }

    public void setCURP(String CURP) {
        this.CURP = CURP;
    }

    public Integer getCalif() {
        return calif;
    }

    public void setCalif(Integer calif) {
        this.calif = calif;
    }

    public Integer getIdMat() {
        return idMat;
    }

    public void setIdMat(Integer idMat) {
        this.idMat = idMat;
    }

    public Integer getIdAlm() {
        return idAlm;
    }

    public void setIdAlm(Integer idAlm) {
        this.idAlm = idAlm;
    }
}

import entity.Alumno;
import entity.Materia;
import entity.Registro;

import java.sql.*;
import java.util.ArrayList;

public class CRUD {
    public int insert(Connection conn, Alumno alumno) throws SQLException {
        String sql = "insert into alumno (nombre, paterno, materno, CURP, grupo, matricula_fk) values (?,?,?,?,?,?) ";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, alumno.getNombre());
        pstmt.setString(2, alumno.getPaterno());
        pstmt.setString(3, alumno.getMaterno());
        pstmt.setString(4, alumno.getCurp());
        pstmt.setString(5, alumno.getGrupo());
        pstmt.setString(6, alumno.getMatricula());
        int i = pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Inserte un registro");
        return i;
    }

    public ArrayList<Alumno> select(Connection conn) throws SQLException {
        ArrayList<Alumno> alumnos = new ArrayList<>();
        Statement myStmt = conn.createStatement();
        ResultSet myRs = myStmt.executeQuery("select idAlmno,nombre, paterno, materno, CURP, grupo, matricula_fk from alumno");

        while (myRs.next()) {
            Alumno alumno = new Alumno();
            alumno.setIdAlumno(myRs.getInt(1));
            alumno.setNombre(myRs.getString(2));
            alumno.setPaterno(myRs.getString(3));
            alumno.setMaterno(myRs.getString(4));
            alumno.setCurp(myRs.getString(5));
            alumno.setGrupo(myRs.getString(6));
            alumno.setMatricula(myRs.getString(7));
            alumnos.add(alumno);
            alumno = null;
        }
        myStmt.close();
        myRs.close();
        System.out.println("Regrese el arreglo de miembros");
        return alumnos;
    }


    public ArrayList<Alumno> selectAct(Connection conn, String clave) throws SQLException {
        ArrayList<Alumno> alumnos = new ArrayList<>();
        Statement myStmt = conn.createStatement();
        ResultSet myRs = myStmt.executeQuery("select CURP from alumno where  CURP =" + "'" +  clave + "'");

        while (myRs.next()) {
            Alumno alumno = new Alumno();
            alumno.setCurp(myRs.getString(1));
            alumnos.add(alumno);
            alumno = null;
        }
        myStmt.close();
        myRs.close();
        System.out.println("Regrese el nombre");
        return alumnos;
    }

    public int update(Connection conn, Alumno alumno, String pk) throws SQLException {
        String sql = "update alumno set nombre = ?, paterno = ?, materno = ? , CURP = ?, grupo = ? where CURP = " + "'"  + pk + "'";
        PreparedStatement pstmt = conn.prepareStatement(sql);

        pstmt.setString(1, alumno.getNombre());
        pstmt.setString(2, alumno.getPaterno());
        pstmt.setString(3, alumno.getMaterno());
        pstmt.setString(4, alumno.getCurp());
        pstmt.setString(5, alumno.getGrupo());

        int i = pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Ya se ha actualizado el registro");
        return i;
    }

    public ArrayList<Registro> selectReg(Connection conn) throws SQLException {
        ArrayList<Registro> registros = new ArrayList<>();
        Statement myStmt = conn.createStatement();
        ResultSet myRs = myStmt.executeQuery("select idReg,CURP, calif, idMateria_fk, idAlumno_fk from registro_academico");

        while (myRs.next()) {
            Registro registro = new Registro();
            registro.setIdReg(myRs.getInt(1));
            registro.setCURP(myRs.getString(2));
            registro.setCalif(myRs.getInt(3));
            registro.setIdMat(myRs.getInt(4));
            registro.setIdAlm(myRs.getInt(5));
            registros.add(registro);
            registro = null;
        }
        myStmt.close();
        myRs.close();
        System.out.println("Regrese el arreglo de registros");
        return registros;
    }



    public int delete(Connection conn, Alumno alumno, Materia materia) throws SQLException {
        String sql = "call eliminar_registro(?,?)";
        PreparedStatement pstmt = conn.prepareStatement(sql);

        pstmt.setInt(1,alumno.getIdAlumno());
        pstmt.setInt(2,materia.getIdMat());
        int i = pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Se ha eliminado el registro");
        return i;
    }
}

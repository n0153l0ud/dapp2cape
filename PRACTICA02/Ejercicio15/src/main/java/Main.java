import entity.Alumno;
import entity.Materia;
import entity.Registro;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws SQLException {
        Scanner entrada = new Scanner(System.in);
        int opcion;

        //Variables para alumno
        String nombre;
        String paterno;
        String materno;
        String curp;
        String grupo;
        String matri;

        String clave;
        String nuevoNombre;
        String nuevoPaterno;
        String nuevoMaterno;
        String nuevoCurp;
        String nuevoGrupo;

        Integer idAlmno;
        Integer idMateria;

        String curpExist = null;

        ArrayList<Alumno> alumnos = new ArrayList<>();
        ArrayList<Alumno> alumnoNombre = new ArrayList<>();
        ArrayList<Registro> registros = new ArrayList<>();

        CRUD e = new CRUD();
        Conexion conn = new Conexion();
        Connection connection = conn.conecta();
        do {
            System.out.println("!Bienvenido!");
            System.out.println("\t Menu de Opciones");
            System.out.println("1.- Insertar un registro en la tabla alumno.");
            System.out.println("2.- Ver los registros de la tabla alumno.");
            System.out.println("3.- Actualizar un registro en la tabla alumno.");
            System.out.println("4.- Eliminar un registro en la tabla alumno.");
            System.out.println("5.- Ver los registros de la tabla registro_academico. ");
            System.out.println("6.- Salir.");

            System.out.println("¿Que deseas hacer? ");
            opcion = Integer.parseInt(entrada.nextLine());

            switch (opcion) {
                case 1:
                    System.out.println("Ingresa el nombre:");
                    nombre = entrada.nextLine();
                    System.out.println("Ingresa el a. paterno:");
                    paterno = entrada.nextLine();
                    System.out.println("Ingresa el a. materno:");
                    materno = entrada.nextLine();
                    System.out.println("Ingresa el CURP:");
                    curp = entrada.nextLine();
                    System.out.println("Ingresa el grupo:");
                    grupo = entrada.nextLine();
                    System.out.println("Ingresa la matricula de la escuela:");
                    matri = entrada.nextLine();

                    Alumno alumno = new Alumno();
                    alumno.setNombre(nombre);
                    alumno.setPaterno(paterno);
                    alumno.setMaterno(materno);
                    alumno.setCurp(curp);
                    alumno.setGrupo(grupo);
                    alumno.setMatricula(matri);

                    e.insert(connection,alumno);
                    break;
                case 2:
                    alumnos = e.select(connection);

                    for (int i = 0; i < alumnos.size(); i++) {
                        System.out.println("--------------------------------------------------");
                        System.out.println("id: " + alumnos.get(i).getIdAlumno());
                        System.out.println("Nombre: " + alumnos.get(i).getNombre());
                        System.out.println("A. Paterno: " + alumnos.get(i).getPaterno());
                        System.out.println("A. Materno: " + alumnos.get(i).getMaterno());
                        System.out.println("CURP: " + alumnos.get(i).getCurp());
                        System.out.println("Grupo: " + alumnos.get(i).getGrupo());
                        System.out.println("Matricula: " + alumnos.get(i).getMatricula());
                    }
                    break;
                case 3:
                    Alumno nuevoAlumno = new Alumno();
                    System.out.println("Nota: En esta opcion se debe de ingresar un CURP valido");
                    System.out.println("Ingresa el CURP del alumno a actualizar:");
                    clave = entrada.nextLine();

                    alumnoNombre = e.selectAct(connection,clave);

                    for (int i = 0; i < alumnoNombre.size(); i++) {
                        curpExist = alumnoNombre.get(i).getCurp();
                    }

                    if(curpExist.equals(clave)) {
                        System.out.println("Ingresa el nuevo nombre:");
                        nuevoNombre = entrada.nextLine();
                        System.out.println("Ingresa el nuevo a. paterno:");
                        nuevoPaterno = entrada.nextLine();
                        System.out.println("Ingresa el  nuevo a. materno:");
                        nuevoMaterno = entrada.nextLine();
                        System.out.println("Ingrese le nuevo CURP: ");
                        nuevoCurp = entrada.nextLine();
                        System.out.println("Ingresa el nuevo grupo:");
                        nuevoGrupo = entrada.nextLine();

                        nuevoAlumno.setNombre(nuevoNombre);
                        nuevoAlumno.setPaterno(nuevoPaterno);
                        nuevoAlumno.setMaterno(nuevoMaterno);
                        nuevoAlumno.setGrupo(nuevoGrupo);

                        e.update(connection,nuevoAlumno,clave);
                    }
                    break;
                case 4:
                    System.out.println("Ingrese el numero del alumno a eliminar: ");
                    idAlmno = entrada.nextInt();
                    System.out.println("Ingrese el numero de la materia del alumno a eliminar: ");
                    idMateria = entrada.nextInt();

                    Alumno alumnoEli = new Alumno();
                    Materia materia = new Materia();

                    alumnoEli.setIdAlumno(idAlmno);
                    materia.setIdMat(idMateria);

                    e.delete(connection,alumnoEli,materia);
                    break;
                case 5:
                    registros = e.selectReg(connection);

                    for (int i = 0; i < registros.size(); i++) {
                        System.out.println("--------------------------------------------------");
                        System.out.println("idReg: " + registros.get(i).getIdReg());
                        System.out.println("CURP: " + registros.get(i).getCURP());
                        System.out.println("Calif: " + registros.get(i).getCalif());
                        System.out.println("idMat: " + registros.get(i).getIdMat());
                        System.out.println("idAlmno: " + registros.get(i).getIdAlm());
                    }
                    break;
                case 6:
                    System.out.println("Programa finalizado");
                    conn.desconecta(connection);
                    break;
                default:
                    System.out.println("Opcion invalida");
            }
        }while (opcion != 6);

    }
}

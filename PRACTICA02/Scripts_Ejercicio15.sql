create database if not exists db_escuela;

use db_escuela;

create table if not exists escuela (
matricula varchar(12) primary key not null,
telefono varchar(10) not null
);

create table if not exists alumno (
idAlmno int primary key not null auto_increment,
nombre varchar(20) not null,
paterno varchar(20) not null,
materno varchar(20) not null,
CURP varchar(14) not null,
grupo varchar(2) not null,
matricula_fk varchar(12) not null,
constraint matricula_idx foreign key (matricula_fk)
references escuela(matricula)
);

create table if not exists materia (
idMat int not null primary key,
nombre varchar(20) not null,
dia varchar(10) not null,
hora time
);

create table if not exists registro_academico (
idReg int not null primary key auto_increment,
CURP varchar(14) not null,
calif int not null,
idMateria_fk int not null,
constraint materia_idx foreign key (idMateria_fk)
references materia(idMat),
idAlumno_fk int not null,
constraint alumno_idx foreign key (idAlumno_fk)
references alumno(idAlmno)
);

SET GLOBAL log_bin_trust_function_creators = 1;

DROP PROCEDURE IF EXISTS calificar_materia;
Delimiter |
create procedure calificar_materia(asistencia integer, trabajos integer, alumno varchar(14), materia integer, id_alumno integer)
comment 'Procedimiento que asigna una calificacion a una materia de un alumno ingresando ciertos datos'
begin
	declare curp varchar(14);
    declare id_materia integer;
    set curp = alumno;
    set id_materia = materia;
    if (asistencia = 100 and trabajos >= 10)
    then
	insert  into registro_academico(CURP, calif, idMateria_fk,idAlumno_fk) values (curp,10,id_materia,id_alumno) ;
    end if;
	if (asistencia >= 80 and trabajos >= 8)
    then
	insert  into registro_academico(CURP, calif, idMateria_fk,idAlumno_fk) values (curp,8,id_materia,id_alumno) ;
    end if;
	if (asistencia >= 60 and trabajos >= 5)
    then
	insert  into registro_academico(CURP, calif, idMateria_fk,idAlumno_fk) values (curp,6,id_materia,id_alumno) ;
    end if;
end |
Delimiter ;

create view calificaciones as 
select concat(t1.nombre,' ', t1.paterno,' ', t1.materno) as nombre_completo, avg(calif) from alumno as T1 
inner join registro_academico where T1.idAlmno = registro_academico.idAlumno_fk group by registro_academico.CURP;

DROP PROCEDURE IF EXISTS eliminar_registro;
Delimiter |
create procedure eliminar_registro (in no_alumno integer, in materia integer)
comment 'Procedimiento para poder eliminar un registro de la tabla alumno'
begin
	declare clave_alumno integer;
    declare clave_materia integer;
    set clave_materia = materia;
    set clave_alumno = no_alumno;
	delete from registro_academico where idAlumno_fk =  clave_alumno and idMateria_fk = clave_materia ;
	delete from alumno where idAlmno = clave_alumno;
end |
Delimiter ;

call eliminar_registro (3,2009);


package com.example.demo.exception;

public class PersonaNotFoundException  extends RuntimeException{
    public PersonaNotFoundException(String message) {
        super(message);
    }
}

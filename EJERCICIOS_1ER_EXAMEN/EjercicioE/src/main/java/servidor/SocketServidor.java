package servidor;

import conexion.Conexion;
import crud.CRUD;
import modelo.Car;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;

public class SocketServidor {
    public static void main(String[] args) {
        int puerto=8000;

        try {

            ServerSocket miServidor = new ServerSocket(puerto);
            Socket miCliente;

            System.out.println("Esperando Conexion...");

            miCliente= miServidor.accept();

            System.out.println("Recibi Conexion");


            //----- Habilitar lectura y escritura del Socket
            BufferedReader lectorSocket;
            PrintWriter escritorSocket;

            InputStreamReader entradaDatos = new InputStreamReader(miCliente.getInputStream());
            lectorSocket= new BufferedReader(entradaDatos );

            escritorSocket= new PrintWriter(miCliente.getOutputStream(),true);
            //----


            String mensajeRespuesta, mensajeRecibido="",lineaRecibida;
            int opcion=0;

            System.out.println("Esperando mensaje....");

            lineaRecibida= lectorSocket.readLine();
            mensajeRecibido=lineaRecibida+"\n";


            if (lineaRecibida.contains("POST"))
            {
                opcion=2;
            }

            System.out.println("Opcion: " + opcion);
            System.out.println(mensajeRecibido);

            if (opcion==2)
            {
                String mensaje = "Llena los datos:";
                escritorSocket.println(mensaje);
                String mensajeRec;

                mensajeRec = lectorSocket.readLine();
                System.out.println(mensajeRec);
                Car car = new Car();

                car.fromJson(mensajeRec);

                Conexion conexion = new Conexion();
                Connection conn = conexion.conecta();
                CRUD crud = new CRUD();

                crud.insert(conn, car);

                conexion.desconecta(conn);
                mensajeRespuesta = "Datos insertados correctamente";
                escritorSocket.println(mensajeRespuesta);
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        System.out.println("FIN del Programa");
    }
}

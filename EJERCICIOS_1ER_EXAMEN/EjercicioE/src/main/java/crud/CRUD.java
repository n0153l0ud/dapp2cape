package crud;

import modelo.Car;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CRUD {
    public int insert(Connection conn, Car car) throws SQLException {
        String sql = "insert into car (model, nombre, color) values (?,?,?) ";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, car.getModel());
        pstmt.setString(2, car.getNombre());
        pstmt.setString(3, car.getColor());
        int i = pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Inserte un registro");
        return i;
    }

}

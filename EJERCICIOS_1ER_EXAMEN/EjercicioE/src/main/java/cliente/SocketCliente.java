package cliente;

import modelo.Car;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketCliente {
    public static void main(String[] args) {
        String opcion;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Socket miCliente = null;
        String host = "localhost";
        int puerto = 8000;
        try {
            System.out.println("Conectando al servidor");

            //Conexion al servidor mediante Socket TCP
            miCliente = new Socket(host, puerto);
            System.out.println("Me conecte al servidor");

            InputStreamReader streamSocket = new InputStreamReader(miCliente.getInputStream());

            BufferedReader lectorSocket = new BufferedReader(streamSocket);

            PrintWriter escritorSocket = new PrintWriter(miCliente.getOutputStream(),true);

            String mensajeTransmitido, mensajeRecibido;

            System.out.println("Escriba POST para hacer un INSERT a la base de datos: ");
            opcion = br.readLine();
            mensajeTransmitido = opcion;
            escritorSocket.println(mensajeTransmitido);

            System.out.println("Enviando mensaje");
            System.out.println("Recibiendo mensaje");
            mensajeRecibido = lectorSocket.readLine();
            System.out.println(mensajeRecibido);
            String modelo, nombre, color;
            Car car = new Car();
            System.out.println("Ingresa el modelo: ");
            modelo = br.readLine();

            System.out.println("Ingresa el nombre: ");
            nombre = br.readLine();

            System.out.println("Ingresa el color: ");
            color = br.readLine();

            System.out.println("Recibiendo mensaje");

            car.setModel(modelo);
            car.setNombre(nombre);
            car.setColor(color);

            mensajeTransmitido = car.toJson();

            escritorSocket.println(mensajeTransmitido);

            mensajeRecibido = lectorSocket.readLine();

            System.out.println(mensajeRecibido);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

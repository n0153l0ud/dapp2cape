package modelo;

import java.util.ArrayList;

public class Car {
    private Integer id;
    private String model;
    private String nombre;
    private String color;
    private Double kilometers = 0.0;
    private Double position = 0.0;

    public Car(){

    }

    public Car(String model, String nombre, String color) {
        this.model = model;
        this.nombre = nombre;
        this.color = color;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    public void moveForward(Double distance){

    }

    public void modeBackward(Double distance){

    }

    public  String toJson() {

        String cadenaJson="{";

        cadenaJson+="\"model\":\""+model + "\",";
        cadenaJson+="\"nombre\":\""+nombre + "\",";
        cadenaJson+="\"color\":\""+color + "\",";
        cadenaJson+="\"kilometers\":\""+kilometers + "\",";
        cadenaJson+="\"position\":\""+position + "\",";

        cadenaJson+="}";

        return cadenaJson;
    }

    public void fromJson(String cadenaJson) {

        cadenaJson=cadenaJson.replace("\"", "");
        cadenaJson=cadenaJson.replace("{", "");
        cadenaJson=cadenaJson.replace("}", "");

        String [] valores = cadenaJson.split(",");

        int inicio=0, fin=0;
        String dato="",campo="";
        for (String contenido : valores) {
            inicio=contenido.indexOf(":") + 1;
            fin= contenido.indexOf(":");

            dato = contenido.substring(inicio);
            campo = contenido.substring(0, fin);

            switch (campo) {
                case "model":
                    model=dato;
                    break;
                case "nombre":
                    nombre=dato;
                    break;
                case "color":
                    color=dato;
                    break;
                case "kilometers":
                    kilometers= Double.valueOf(dato);
                    break;
                case "position":
                    position= Double.valueOf(dato);
                    break;
            }
        }
    }


}

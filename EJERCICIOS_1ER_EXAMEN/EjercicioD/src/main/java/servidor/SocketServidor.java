package servidor;

import conexion.Conexion;
import crud.CRUD;
import modelo.Car;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;

public class SocketServidor {
    public static void main(String[] args) {
        int puerto = 8000;

        ArrayList<Car> carrs = new ArrayList<>();
        try {

            ServerSocket miServidor = new ServerSocket(puerto);
            Socket miCliente;

            System.out.println("Esperando Conexion...");

            miCliente = miServidor.accept();

            System.out.println("Recibi Conexion");


            //----- Habilitar lectura y escritura del Socket
            BufferedReader lectorSocket;
            PrintWriter escritorSocket;

            InputStreamReader entradaDatos = new InputStreamReader(miCliente.getInputStream());
            lectorSocket = new BufferedReader(entradaDatos);

            escritorSocket = new PrintWriter(miCliente.getOutputStream(), true);
            //----

            String mensajeRespuesta, mensajeRecibido = "", lineaRecibida;
            int opcion = 0, totalContenido = 0;

            System.out.println("Esperando mensaje....");

            lineaRecibida = lectorSocket.readLine();
            mensajeRecibido = lineaRecibida + "\n";


            if (lineaRecibida.contains("GET")) {
                opcion = 1;
            }

            System.out.println("Opcion: " + opcion);
            System.out.println(mensajeRecibido);
            if (opcion == 1) {
                Conexion conexion = new Conexion();
                Connection conn = conexion.conecta();
                CRUD crud = new CRUD();

                carrs = crud.select(conn);
                String contenido = Car.toXmlList(carrs);
                System.out.println(contenido);

                escritorSocket.println(contenido);

                conexion.desconecta(conn);
            }
        }catch(Exception ex)            {
            System.out.println(ex.getMessage());
        }
        System.out.println("FIN del Programa");
    }
}
package crud;

import modelo.Car;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CRUD {
    public ArrayList<Car> select(Connection conn) throws SQLException {
        ArrayList<Car> carros = new ArrayList<>();
        Statement myStmt = conn.createStatement();
        ResultSet myRs = myStmt.executeQuery("select id_car,model, nombre, color from car");

        while (myRs.next()) {
            Car car = new Car();
            car.setId(myRs.getInt(1));
            car.setModel(myRs.getString(2));
            car.setNombre(myRs.getString(3));
            car.setColor(myRs.getString(4));
            carros.add(car);
            car = null;
        }
        myStmt.close();
        myRs.close();
        System.out.println("Regrese el arreglo de carros");
        return carros;
    }

}

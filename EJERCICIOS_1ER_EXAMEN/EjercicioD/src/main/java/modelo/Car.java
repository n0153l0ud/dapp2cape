package modelo;

import java.util.ArrayList;

public class Car {
    private Integer id;
    private String model;
    private String nombre;
    private String color;
    private Double kilometers = 0.0;
    private Double position = 0.0;

    public Car(){

    }

    public Car(String model, String nombre, String color) {
        this.model = model;
        this.nombre = nombre;
        this.color = color;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    public void moveForward(Double distance){

    }

    public void modeBackward(Double distance){

    }

    public String toXml()
    {
        String cadenaXml="<Car>";
        cadenaXml+="<id>" + id +"</id>";
        cadenaXml+="<model>" + model +"</model>";
        cadenaXml+="<nombre>" + nombre +"</nombre>";
        cadenaXml+="<color>" + color +"</color>";
        cadenaXml+="<kilometers>" + kilometers +"</kilometers>";
        cadenaXml+="<position>" + position +"</position>";

        cadenaXml += "</Car>";

        return cadenaXml;
    }

    public static String toXmlList(ArrayList<Car> cars)
    {
        String cadenaXml="<Cars>";

        for (Car car:cars)
        {
            cadenaXml += car.toXml();
        }

        cadenaXml+="</Cars>";
        return cadenaXml;
    }

}

import java.net.*;
import java.io.*;
import java.util.Scanner;


public class SocketCliente {
    private Socket socket;
    private DataInputStream bufferDeEntrada = null;
    private DataOutputStream bufferDeSalida = null;
    Scanner teclado = new Scanner(System.in);
    final String CONDICION_TERMINACION = "Adios";

    /**
     * Metodo que como su nombre lo indica sirve para levantar la conexión
     * recibiendo como parametros:
     * @param ip un objeto de tipo String el cual es la dirección ip
     * @param puerto una variable de tipo int el cual es el puerto
     */
    public void levantarConexion(String ip, int puerto) {
        try {
            socket = new Socket(ip, puerto);
            mostrarTexto("Conectado a :" + socket.getInetAddress().getHostName());
        } catch (Exception e) {
            mostrarTexto("Excepción al levantar conexión: " + e.getMessage());
            System.exit(0);
        }
    }

    /**
     * Metodo que sirve para mostrar el texto actual
     * recibe como parametro:
     * @param s un objeto de tipo String el cual es el texto
     */
    public static void mostrarTexto(String s) {
        System.out.println(s);
    }

    /**
     * Metodo que permite abriri los flujos para poder enviar y recibir mensajes
     */
    public void abrirFlujos() {
        try {
            bufferDeEntrada = new DataInputStream(socket.getInputStream());
            bufferDeSalida = new DataOutputStream(socket.getOutputStream());
            bufferDeSalida.flush();
        } catch (IOException e) {
            mostrarTexto("Error en la apertura de flujos");
        }
    }

    /**
     * Metodo que sirve para enviar los datos ingresados por teclado
     * recibe como parametro:
     * @param s un objeto de tipo String el cual es el texto ingresado
     */
    public void enviar(String s) {
        try {
            bufferDeSalida.writeUTF(s);
            bufferDeSalida.flush();
        } catch (IOException e) {
            mostrarTexto("IOException on enviar");
        }
    }

    /**
     * Metodo que sirve para cerrar la conexión
     */
    public void cerrarConexion() {
        try {
            bufferDeEntrada.close();
            bufferDeSalida.close();
            socket.close();
            mostrarTexto("Conexión terminada");
        } catch (IOException e) {
            mostrarTexto("IOException on cerrarConexion()");
        }finally{
            System.exit(0);
        }
    }

    /**
     * Metodo que sirve para levantar la conexión,
     * abrir el flujo de datos y a su vez comenzar a recibir los datos
     * recibe como parametros:
     * @param ip un objeto de tipo String el cual es la dirección ip
     * @param puerto una variable de tipo int el cual es el puerto
     */
    public void ejecutarConexion(String ip, int puerto) {
        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    levantarConexion(ip, puerto);
                    abrirFlujos();
                    recibirDatos();
                } finally {
                    cerrarConexion();
                }
            }
        });
        hilo.start();
    }

    /**
     * Metodo que sirve para mostrar los datos recibiddos
     */
    public void recibirDatos() {
        String st = "";
        try {
            do {
                st = (String) bufferDeEntrada.readUTF();
                mostrarTexto("\n[Servidor] => " + st);
                System.out.print("\n[Usted] => ");
            } while (!st.equals(CONDICION_TERMINACION));
        } catch (IOException e) {}
    }

    /**
     * Metodo el cual sirve para escribir los datos ingresador por el
     * usuario en turno
     */
    public void escribirDatos() {
        String entrada = "";
        while (true) {
            System.out.print("[Usted] => ");
            entrada = teclado.nextLine();
            if(entrada.length() > 0)
                enviar(entrada);
        }
    }

    /**
     * Metodo que sirve para probar la clase
     * @param argumentos
     */
    public static void main(String[] argumentos) {
        SocketCliente socketCliente = new SocketCliente();
        Scanner escaner = new Scanner(System.in);
        mostrarTexto("Ingresa la IP: [localhost por defecto] ");
        String ip = escaner.nextLine();
        if (ip.length() <= 0) ip = "localhost";

        mostrarTexto("Puerto: [5050 por defecto] ");
        String puerto = escaner.nextLine();
        if (puerto.length() <= 0) puerto = "5050";
        socketCliente.ejecutarConexion(ip, Integer.parseInt(puerto));
        socketCliente.escribirDatos();
    }
}
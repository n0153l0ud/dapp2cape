import java.net.*;
import java.io.*;
import java.util.Scanner;


public class SocketServidor {

    private Socket socket;
    private ServerSocket serverSocket;
    private DataInputStream bufferDeEntrada = null;
    private DataOutputStream bufferDeSalida = null;
    Scanner escaner = new Scanner(System.in);
    final String CONDICION_TERMINACION = "Adios";

    /**
     * Metodo que como su nombre lo indica sirve para levantar la conexión
     * recibiendo como parametro:
     * @param puerto una variable de tipo int el cual es el puerto
     */
    public void levantarConexion(int puerto) {
        try {
            serverSocket = new ServerSocket(puerto);
            mostrarTexto("Esperando conexión entrante en el puerto " + String.valueOf(puerto) + "...");
            socket = serverSocket.accept();
            mostrarTexto("Conexión establecida con: " + socket.getInetAddress().getHostName() + "\n\n\n");
        } catch (Exception e) {
            mostrarTexto("Error en levantarConexion(): " + e.getMessage());
            System.exit(0);
        }
    }

    /**
     * Metodo que permite abriri los flujos para poder enviar y recibir mensajes
     */
    public void flujos() {
        try {
            bufferDeEntrada = new DataInputStream(socket.getInputStream());
            bufferDeSalida = new DataOutputStream(socket.getOutputStream());
            bufferDeSalida.flush();
        } catch (IOException e) {
            mostrarTexto("Error en la apertura de flujos");
        }
    }

    /**
     * Metodo que sirve para mostrar los datos recibiddos
     */
    public void recibirDatos() {
        String st = "";
        try {
            do {
                st = (String) bufferDeEntrada.readUTF();
                mostrarTexto("\n[SocketCliente] => " + st);
                System.out.print("\n[Usted] => ");
            } while (!st.equals(CONDICION_TERMINACION));
        } catch (IOException e) {
            cerrarConexion();
        }
    }

    /**
     * Metodo que sirve para enviar los datos ingresados por teclado
     * recibe como parametro:
     * @param s un objeto de tipo String el cual es el texto ingresado
     */
    public void enviar(String s) {
        try {
            bufferDeSalida.writeUTF(s);
            bufferDeSalida.flush();
        } catch (IOException e) {
            mostrarTexto("Error en enviar(): " + e.getMessage());
        }
    }

    /**
     * Metodo que sirve para mostrar el texto actual
     * recibe como parametro:
     * @param s un objeto de tipo String el cual es el texto
     */
    public static void mostrarTexto(String s) {
        System.out.print(s);
    }

    public void escribirDatos() {
        while (true) {
            System.out.print("[Usted] => ");
            enviar(escaner.nextLine());
        }
    }

    /**
     * Metodo que sirve para cerrar la conexión
     */
    public void cerrarConexion() {
        try {
            bufferDeEntrada.close();
            bufferDeSalida.close();
            socket.close();
        } catch (IOException e) {
            mostrarTexto("Excepción en cerrarConexion(): " + e.getMessage());
        } finally {
            mostrarTexto("Conversación finalizada....");
            System.exit(0);

        }
    }

    /**
     * Metodo que sirve para levantar la conexión,
     * abrir el flujo de datos y a su vez comenzar a recibir los datos
     * recibe como parametro:
     * @param puerto una variable de tipo int el cual es el puerto
     */
    public void ejecutarConexion(int puerto) {
        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        levantarConexion(puerto);
                        flujos();
                        recibirDatos();
                    } finally {
                        cerrarConexion();
                    }
                }
            }
        });
        hilo.start();
    }

    /**
     * Metodo que sirve para probar la clase
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        SocketServidor s = new SocketServidor();
        Scanner sc = new Scanner(System.in);

        mostrarTexto("Ingresa el puerto [5050 por defecto]: ");
        String puerto = sc.nextLine();
        if (puerto.length() <= 0) puerto = "5050";
        s.ejecutarConexion(Integer.parseInt(puerto));
        s.escribirDatos();
    }
}
import java.io.FileWriter;

public class Escritura {
    public static void main(String[] args) {

        String[] lineas = { "Perro", "Gato", "Pez", "Ave", "Anfibio", "Reptil", "Insecto", "..." };

        /** FORMA 1 DE ESCRITURA **/
        FileWriter fichero = null;
        try {

            fichero = new FileWriter("fichero_escritura.txt");

            // Escribimos linea a linea en el fichero
            for (String linea : lineas) {
                fichero.write(linea + "\n");
            }

            fichero.close();

        } catch (Exception ex) {
            System.out.println("Mensaje de la excepción: " + ex.getMessage());
        }
    }
}

package cliente;

import modelo.Persona;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Cliente {
    public static void main(String[] args) {
        Socket miCliente = null;
        String host = "localhost";
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int puerto = 8000;
        try {
            System.out.println("Conectando al servidor");

            miCliente = new Socket(host, puerto);
            System.out.println("Me conecte al servidor");

            InputStreamReader streamSocket = new InputStreamReader(miCliente.getInputStream());

            BufferedReader lectorSocket = new BufferedReader(streamSocket);

            PrintWriter escritorSocket = new PrintWriter(miCliente.getOutputStream(),true);

            String mensajeTransmitido, mensajeRecibido;

            String nombre, paterno, materno, genero;
            Integer edad;

            Persona persona = new Persona();

            System.out.println("Ingresa el nombre:");
            nombre = br.readLine();

            System.out.println("Ingresa el A. Paterno:");
            paterno = br.readLine();

            System.out.println("Ingresa el A. Materno:");
            materno = br.readLine();

            System.out.println("Ingresa la edad:");
            edad = Integer.valueOf(br.readLine());

            System.out.println("Ingresa el genero:");
            genero = br.readLine();

            persona.setNombre(nombre);
            persona.setPaterno(paterno);
            persona.setMaterno(materno);
            persona.setEdad(edad);
            persona.setGenero(genero);

            mensajeTransmitido = persona.toJson();

            System.out.println("Enviando mensaje");

            escritorSocket.println(mensajeTransmitido);

            System.out.println("Recibiendo mensaje");

            mensajeRecibido = lectorSocket.readLine();

            System.out.println(mensajeRecibido);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

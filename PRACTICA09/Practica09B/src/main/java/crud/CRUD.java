package crud;

import modelo.Persona;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CRUD {
    public int insert(Connection conn, Persona persona) throws SQLException {
        String sql = "insert into persona(id_persona, nombre, paterno, materno, edad, genero)\n" +
                "     values(null,?,?,?,?,?)";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, persona.getNombre());
        pstmt.setString(2, persona.getPaterno());
        pstmt.setString(3, persona.getMaterno());
        pstmt.setString(4, String.valueOf(persona.getEdad()));
        pstmt.setString(5, persona.getGenero());
        int i = pstmt.executeUpdate();
        pstmt.close();
        return 0;
    }
}

package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    Connection conn;

    public Connection conecta() throws SQLException {
        conn = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/practica10?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "usuario", "octane");
        if (conn != null) {
            return conn;
        } else {
            return null;
        }
    }

    public void desconecta(Connection conexion) throws SQLException {
        conexion.close();
    }
}

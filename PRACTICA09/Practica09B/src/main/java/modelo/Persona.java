package modelo;

import com.google.gson.Gson;

import java.io.IOException;

public class Persona {
    private Integer id;
    private String nombre;
    private String paterno;
    private String materno;
    private Integer edad;
    private String genero;

    public Persona() {
    }

    public Persona(Integer id, String nombre, String paterno, String materno, Integer edad, String genero) {
        this.id = id;
        this.nombre = nombre;
        this.paterno = paterno;
        this.materno = materno;
        this.edad = edad;
        this.genero = genero;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String toJson() {

        String cadenaJson="{";
        cadenaJson+="\"nombre\":\""+nombre + "\",";
        cadenaJson+="\"paterno\":\""+paterno + "\",";
        cadenaJson+="\"materno\":\""+materno + "\",";
        cadenaJson+="\"edad\":\""+edad + "\",";
        cadenaJson+="\"genero\":\""+genero + "\"";
        cadenaJson+="}";

        return cadenaJson;
    }

    public Persona deserializacionJSON(String json){
        String jsonString = json;

        Gson gson = new Gson();

        Persona persona = gson.fromJson(jsonString, Persona.class);

        System.out.println("Nombre: " + persona.getNombre());
         System.out.println("Paterno: " + persona.getPaterno());
         System.out.println("Materno: " + persona.getMaterno());
         System.out.println("Edad:" + persona.getEdad());
         System.out.println("Genero:" + persona.getGenero());
         return persona;
    }




}

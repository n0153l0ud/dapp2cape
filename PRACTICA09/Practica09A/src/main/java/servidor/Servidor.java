package servidor;

import conexion.Conexion;
import crud.CRUD;
import modelo.Persona;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;

public class Servidor {
    public static void main(String[] args) throws SQLException {
        int puerto = 8000;
        try {
            ServerSocket miServidor = new ServerSocket(puerto);
            Socket miCliente = null;

            System.out.println("Esperando conexion");

            miCliente = miServidor.accept();

            System.out.println("Conexion aceptada");

            InputStreamReader streamSocket = new InputStreamReader(miCliente.getInputStream());

            BufferedReader lectorSocket = new BufferedReader(streamSocket);

            PrintWriter escritorSocket = new PrintWriter(miCliente.getOutputStream(), true);

            String mensajeRecibido, mensajeEnviado;

            System.out.println("Esperando mensaje");

            mensajeRecibido = lectorSocket.readLine();

            System.out.println(mensajeRecibido);

            System.out.println("Mandando información");

            Persona persona = new Persona();

            persona = persona.deserializacionXML(mensajeRecibido);

            Conexion conexion = new Conexion();
            Connection conn = conexion.conecta();
            CRUD crud = new CRUD();

            crud.insert(conn,persona);

            /**System.out.println("Nombre: " + persona.getNombre());
            System.out.println("Paterno: " + persona.getPaterno());
            System.out.println("Materno: " + persona.getMaterno());
            System.out.println("Edad:" + persona.getEdad());
            System.out.println("Genero:" + persona.getGenero());*/

            mensajeEnviado = "Se han insertado los datos correctamente";
            escritorSocket.println(mensajeEnviado);
            System.out.println(mensajeEnviado);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

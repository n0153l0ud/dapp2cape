package modelo;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;

public class Persona {
    private Integer id;
    private String nombre;
    private String paterno;
    private String materno;
    private Integer edad;
    private String genero;

    public Persona() {
    }

    public Persona(Integer id, String nombre, String paterno, String materno, Integer edad, String genero) {
        this.id = id;
        this.nombre = nombre;
        this.paterno = paterno;
        this.materno = materno;
        this.edad = edad;
        this.genero = genero;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String toXml() {
        String cadenaXml="<Persona>";
        cadenaXml+="<nombre>" + nombre +"</nombre>";
        cadenaXml+="<paterno>" + paterno +"</paterno>";
        cadenaXml+="<materno>" + materno +"</materno>";
        cadenaXml+="<edad>" + edad +"</edad>";
        cadenaXml+="<genero>" + genero +"</genero>";
        cadenaXml += "</Persona>";

        return cadenaXml;

    }
    public Persona deserializacionXML(String xml) throws IOException {
            XmlMapper xmlMapper = new XmlMapper();

            // read file and put contents into the string
            String readContent = new String(xml);

            // deserialize from the XML into a Phone object
            Persona persona = xmlMapper.readValue(readContent, Persona.class);

            System.out.println("Nombre: " + persona.getNombre());
            System.out.println("Paterno: " + persona.getPaterno());
            System.out.println("Materno: " + persona.getMaterno());
            System.out.println("Edad:" + persona.getEdad());
            System.out.println("Genero:" + persona.getGenero());

            return  persona;

    }


}

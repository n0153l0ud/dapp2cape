import com.google.gson.Gson;
import modelo.Persona;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SocketCliente {
    public static void main(String[] args) {
        String opcion;
        Gson gson = new Gson();
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        //Socket miCliente = null;
        //String host = "localhost";
        int puerto = 8080;
        try {
            System.out.println("Conectando al servidor");
            //Conexion al servidor mediante Socket TCP
            //miCliente = new Socket(host, puerto);
            System.out.println("Me conecte al servidor");

            String mensajeRecibido;

            System.out.println("Ingresa la opcion a realizar: ");
            System.out.println("1.- GET");
            System.out.println("2.- POST");
            System.out.println("3.- PUT");
            System.out.println("4.- DELETE");
            opcion = br.readLine();
            if(opcion.equals("GET")){
                System.out.println("Enviando mensaje");
                HttpGet httpGet = new HttpGet("http://localhost:8080/personas");
                //mensajeTransmitido = "GET http://localhost:8080/personas";
                //mensajeTransmitido = httpGet.toString();
                //escritorSocket.println(httpGet);
                response = client.execute(httpGet);
                System.out.println("Recibiendo mensaje");

                BufferedReader lectorSocket = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                mensajeRecibido = lectorSocket.readLine();
                System.out.println("La lista de personas es:");
                System.out.println(mensajeRecibido);
            }
            else if(opcion.equals("POST"))
            {
                System.out.println("Llene los datos:");
                String nombre, genero, estatus;
                Integer edad;
                Persona persona = new Persona();
                System.out.println("Ingresa el nombre: ");
                nombre = br.readLine();

                System.out.println("Ingresa la edad: ");
                edad = Integer.parseInt(br.readLine());

                System.out.println("Ingresa el genero: ");
                genero = br.readLine();

                System.out.println("Ingresa el estatus");
                estatus = br.readLine();

                System.out.println("Recibiendo mensaje");

                persona.setNombre(nombre);
                persona.setEdad(edad);
                persona.setGenero(genero);
                persona.setEstatus(estatus);

                StringEntity stringEntity = new StringEntity(gson.toJson(persona));

                System.out.println(stringEntity);

                HttpPost httpPost = new HttpPost("http://localhost:8080");

                httpPost.setEntity(stringEntity);

                httpPost.setHeader("Content-Type","application/json");

                response = client.execute(httpPost);

                String result;

                result = EntityUtils.toString(response.getEntity());

                System.out.println(result);
            }
            else if (opcion.equals("PUT"))
            {
                String nombreNuevo, generoNuevo, estatusNuevo;
                Integer edadNueva, id;

                Persona personaNueva = new Persona();

                System.out.println("Ingrese el id del usuario a actualizar:");
                id = Integer.parseInt(br.readLine());

                System.out.println("Ingresa el nuevo nombre: ");
                nombreNuevo = br.readLine();

                System.out.println("Ingresa la nueva edad: ");
                edadNueva = Integer.parseInt(br.readLine());

                System.out.println("Ingresa el nuevo genero: ");
                generoNuevo = br.readLine();

                System.out.println("Ingresa el nuevo estatus");
                estatusNuevo = br.readLine();

                personaNueva.setNombre(nombreNuevo);
                personaNueva.setEdad(edadNueva);
                personaNueva.setGenero(generoNuevo);
                personaNueva.setEstatus(estatusNuevo);

                StringEntity stringEntity = new StringEntity(gson.toJson(personaNueva));

                System.out.println(stringEntity);

                HttpPut httpPut = new HttpPut("http://localhost:8080" + "/"+ id);

                httpPut.setEntity(stringEntity);

                httpPut.setHeader("Content-Type","application/json");

                response = client.execute(httpPut);

                String result;

                result = EntityUtils.toString(response.getEntity());

                System.out.println(result);
            }
            else if ((opcion.equals("DELETE")))
            {
                Integer id;
                System.out.println("Ingresa el id de la persona a eliminar:");
                id = Integer.valueOf(br.readLine());

                HttpDelete httpDelete = new HttpDelete("http://localhost:8080" + "/" + id);

                response = client.execute(httpDelete);

                String result;

                result = EntityUtils.toString(response.getEntity());

                System.out.println(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

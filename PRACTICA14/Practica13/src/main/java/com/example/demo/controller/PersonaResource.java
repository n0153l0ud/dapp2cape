package com.example.demo.controller;

import com.example.demo.exception.PersonaNotFoundException;
import com.example.demo.modelo.Persona;
import com.example.demo.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PersonaResource {

    @Autowired
    private PersonaRepository personaRepository;

    @GetMapping("/personas")
    public List<Persona> obtenerTodasLasPersonas() {
        return personaRepository.findAll();
    }

    @GetMapping("/personas/{id}")
    public Persona obtenerPersona(@PathVariable long id) {
        Optional<Persona> persona = personaRepository.findById(id);

        if (!persona.isPresent())
            throw new PersonaNotFoundException("id-" + id);
        return persona.get();
    }

    /*
    @DeleteMapping("/personas/{id}")
    public void eliminarPersona(@PathVariable long id) {
        personaRepository.deleteById(id);
    }
    */

    @DeleteMapping(value = "/{id}")
    public String  eliminarPersona(@PathVariable long id) {
        personaRepository.deleteById(id);
        return "Registro eliminado correctamente";
    }

    /*
    @PostMapping("/personas")
    public ResponseEntity<Object> crearPersona(@RequestBody Persona persona) {
        Persona guardarPersona = personaRepository.save(persona);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(guardarPersona.getId_persona()).toUri();
        return ResponseEntity.created(location).build();

    }
    */

    @PostMapping(value = "/",consumes = "application/json",produces = "application/json")
    public String crearPersona(@RequestBody Persona persona) {
        personaRepository.save(persona);
        return "Registro Exitoso";
    }

    /*@PutMapping("/personas/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Persona persona, @PathVariable long id) {

        Optional<Persona> personaOptional = personaRepository.findById(id);

        if (!personaOptional.isPresent())
            return ResponseEntity.notFound().build();

        persona.setId_persona(id);

        personaRepository.save(persona);

        return ResponseEntity.noContent().build();
    }*/

    @PutMapping(value = "/{id}",consumes = "application/json",produces = "application/json")
    public String actualizarPersona(@RequestBody Persona persona, @PathVariable long id) {
        Optional<Persona> personaOptional = personaRepository.findById(id);

        if (!personaOptional.isPresent())
            return "La persona no existe";
        persona.setId_persona(id);
        personaRepository.save(persona);
        return "Actualización Exitosa";
    }
}

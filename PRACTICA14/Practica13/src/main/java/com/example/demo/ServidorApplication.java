package com.example.demo;

import com.example.demo.controller.PersonaResource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

@SpringBootApplication
public class ServidorApplication {

    public static void main(String[] args)  {
        SpringApplication.run(ServidorApplication.class, args);
    }

}

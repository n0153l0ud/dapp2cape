/**
 *UNIVERSIDAD IBEROAMERICANA
 *	TSU en Software
 *	Profesor:	Esp. Omar VazquezGonzalez
 *	Alumno:		Emilio Caballero Paredes
 *	Asignatura:	Desarrollo de Aplicaciones II
 *	Fecha:		17/01/2021
 *	Programa:   Ejercicio 2
 *	Descripcion:	Tablas de multiplicar del 1 al 10
 *
 */

public class Ejercicio02 {

    public static void main(String[] args) {

        System.out.println("Función: ");
        tablas10();

    }
    public static void tablas10(){
        for (int i = 1; i <= 10 ; i++) {
            System.out.println("-------------------------------");
            for (int j = 1; j <= 10 ; j++) {
                System.out.println("La tabla de multiplicar del " + i + " es: ");
                System.out.println(i + " * " + j + " = " + i * j);
            }
        }
    }
}

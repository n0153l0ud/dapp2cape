public class Ejercicio03 {
    Double radio;

    public static double metodoValor(Double radio){
        double area = 0.0;
        final double PI = 3.1416;
        radio = 20.20;
        area = PI * radio * radio;
        return area;
    }
    public void cambiarRadio(Double nuevoRadio){
        this.radio = nuevoRadio;
    }

    public static double metodoReferencia(Ejercicio03 circulo){
        double area = 0.0;
        final double PI = 3.1416;
        circulo.cambiarRadio(10.10);
        area = PI * circulo.radio * circulo.radio;
        return area;
    }

    public static void main(String[] args) {
        System.out.println("Radio 1: 13.15");
        System.out.println(metodoValor(13.15));

        Ejercicio03 circulo = new Ejercicio03();
        System.out.println("Radio 1: ");
        circulo.cambiarRadio(7.7);
        System.out.println(circulo.radio);

        System.out.println("Area con ese radio: " + 3.1416 *  circulo.radio * circulo.radio);
        System.out.println("Area 2:");
        System.out.println(metodoReferencia(circulo));

    }
}


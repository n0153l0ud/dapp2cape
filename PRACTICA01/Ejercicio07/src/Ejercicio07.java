public class Ejercicio07 {
    public static void forma1(int numero){
        int iFact = 1;
        for (int x = 2; x <= numero; x++)
            iFact = iFact * x;
        System.out.println("El factorial del número: " + numero +  " es: " + Integer.toString(iFact));
    }

    public static int fomra2(int numero){
        if(numero == 0){
            return 1;
        }
        else
            return numero * fomra2(numero - 1);
    }

    public static void main(String[] args) {
        forma1(5);
        System.out.println();
        System.out.println(fomra2(5));
    }
}

public class Ejercicio05 {
    public static void oBurbuja(int valores[]){
        final int N = valores.length;
        for(int i=N-1; i>0; i--) {
            for(int e = 0; e <i; e++) {
                if(valores[e]>valores[e +1]) {
                    int tmp = valores[e];
                    valores[e] = valores[e +1];
                    valores[e +1]  = tmp;
                }
            }
        }
    }

    public static void imprimirArray(int valores[]){
        for (int i = 0; i < valores.length; i++) {
            System.out.println(valores[i]);
        }
    }
    public static void main(String[] args) {
        int x[] = {9,6,7,2,5,3};
        oBurbuja(x);
        imprimirArray(x);
    }
}

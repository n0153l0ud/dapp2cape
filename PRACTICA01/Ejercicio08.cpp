#include <iostream>

using namespace std;

struct numero
{
  int valor;
  numero *derecha;
  numero *izquierda;


};
bool hay_hijo (int n);
numero *ConstruyendoArbol ();
void imprimirArbolPre (numero * p);
void imprimirArbolPost (numero * p);
void imprimirArbolEntre (numero * p);
void eliminaNodos (numero * p);
int obtenerNivel (numero * p);
bool buscar (int n, numero * nodo);


int
main ()
{
  struct numero *p;
  p = NULL;
  int op;
  int i = 0;
  do
    {
      cout << "Indica la accion que quieres hacer: " << endl;
      cout << "1.- Llenar Arbol." << endl;
      cout << "2.- Impresion Pre-orden ." << endl;
      cout << "3.- Impresion Post-orden." << endl;
      cout << "4.- Impresion Entre-orden." << endl;
      cout << "5.- Eliminacion de nodos. " << endl;
      cout << "6.- Buscar un valor. " << endl;
      cout << "7.- Salir. " << endl;
      cout << "Indica la accion que quieres hacer: " << endl;
      cin >> op;
      switch (op)
	{
	case 1:
	  p = ConstruyendoArbol ();

	  break;
	case 2:
	  imprimirArbolPre (p);

	  break;
	case 3:
	  imprimirArbolPost (p);

	  break;
	case 4:
	  imprimirArbolEntre (p);

	  break;
	case 5:
	  eliminaNodos (p);
	  p = NULL;
	  break;
	  case 6:
	  	  int numero;
	  	  cout << "Ingresa el valor a buscar";
	  	  cin >> numero;
	  	  if(buscar(numero, p)) {
	  	     cout << "El valor se encontro "; 
	  	  }else{
	  	      cout <<"No existe ese valor";
	  	  }

	  break;
	case 7:
	  cout << "Programa finalizado";
	  break;
	default:
	  cout << "Opcion Invalida.";


	}
    }
  while (op != 7);
  return 0;
}

bool
hay_hijo (int n)
{
  char c;
  char *lado[2] = { "izq", "der" };
  cout << "hay un hijo " << lado[n] << "?";
  cin >> c;
  return c == 's' || c == 'S';

}


numero *
ConstruyendoArbol ()
{
  numero *raiz;
  raiz = new numero;
  cout << "Dame valor: " << endl;
  cin >> raiz->valor;
  raiz->izquierda = NULL;
  raiz->derecha = NULL;
  if (hay_hijo (0))
    raiz->izquierda = ConstruyendoArbol ();
  if (hay_hijo (1))
    raiz->derecha = ConstruyendoArbol ();
  return raiz;
}

void
imprimirArbolPre (numero * nodo)
{
  if (nodo == NULL)
    return;
  cout << "Valor: " << nodo->valor << endl;
  imprimirArbolPre (nodo->izquierda);
  imprimirArbolPre (nodo->derecha);
}

void
imprimirArbolPost (numero * nodo)
{
  if (nodo == NULL)
    return;
  imprimirArbolPost (nodo->izquierda);
  imprimirArbolPost (nodo->derecha);
  cout << "Valor: " << nodo->valor << endl;
}

void
imprimirArbolEntre (numero * nodo)
{
  if (nodo == NULL)
    return;
  imprimirArbolEntre (nodo->izquierda);
  cout << "Valor: " << nodo->valor << endl;
  imprimirArbolEntre (nodo->derecha);

}

void
eliminaNodos (numero * p)
{
  if (p == NULL)
    return;
  eliminaNodos (p->izquierda);
  eliminaNodos (p->derecha);
  delete p;
}


bool
buscar (int n, numero * nodo)
{
  if (nodo == NULL){
   return false;
  }
  else if (nodo->valor == n){
    return true;
  }
  else if (n < nodo->valor){
    return buscar (n, nodo->izquierda);
  }
  else{
    return buscar (n, nodo->derecha);
  }
}

public class Ejercicio06 {

    public static void metodoQ(int valores[],int izq, int der){
        int pivote = valores[izq];

        int i = izq;
        int j = der;
        int aux;

        while(i<j)
        {
            while (valores[i] <= pivote && i < j)
                i++;
            while (valores[j] > pivote)
                j--;
            if (i<j)
            {
                aux = valores[i];
                valores[i]= valores[j];
                valores[j]=aux;
            }
        }

        valores[izq] = valores[j];
        valores[j] = pivote;

        if (izq < j-1)
            metodoQ(valores,izq,j-1);
        if (j+1 < der)
            metodoQ(valores,j+1,der);
    }
    public static void imprimirArrego(int valores[]){
        for (int i = 0; i < valores.length; i++) {
            System.out.println(valores[i]);
        }
    }
    public static void main(String[] args) {
        int x[]={9,6,7,2,5,3};
        metodoQ(x,0,x.length-1);
        imprimirArrego(x);
    }
}

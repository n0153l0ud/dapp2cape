package PersonasProyecto;

public class EjemploPersona {
	public static void main(String[] args) {
		Persona persona1 = new Persona("Diego", 7, "Masculino");
		System.out.println(persona1);
		System.out.println(persona1.toXml());
		
		Persona persona2 = new Persona();
		
		persona2.fromXml("<Persona><nombre>Gustavo Caballero</nombre><edad>24</edad><genero>Masculino</genero><estatus>true</estatus></Persona>");
		
		System.out.println();
		System.out.println(persona2);
		System.out.println(persona2.toXml());
		
		
		System.out.println();
		System.out.println(persona2);
		System.out.println(persona2.toJson());
		
		
		persona2.fromJson("{\"nombre\":\"Estela\",\"edad\":45,\"genero\":\"Femenino\",\"estatus\":false}");
		System.out.println(persona2);
	}
}

package crud;

import modelo.Persona;

import java.sql.*;
import java.util.ArrayList;

public class CRUD {
    public int insert(Connection conn, Persona persona) throws SQLException {
        String sql = "insert into persona (nombre, edad, genero, estatus) values (?,?,?,?) ";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, persona.getNombre());
        pstmt.setInt(2, persona.getEdad());
        pstmt.setString(3, persona.getGenero());
        pstmt.setString(4, persona.getEstatus());
        int i = pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Inserte un registro");
        return i;
    }

    public ArrayList<Persona> select(Connection conn) throws SQLException {
        ArrayList<Persona> personas = new ArrayList<>();
        Statement myStmt = conn.createStatement();
        ResultSet myRs = myStmt.executeQuery("select id_persona,nombre, edad, genero,estatus from persona");

        while (myRs.next()) {
            Persona persona = new Persona();
            persona.setId(myRs.getInt(1));
            persona.setNombre(myRs.getString(2));
            persona.setEdad(myRs.getInt(3));
            persona.setGenero(myRs.getString(4));
            persona.setEstatus(myRs.getString(5));
            personas.add(persona);
            persona = null;
        }
        myStmt.close();
        myRs.close();
        System.out.println("Regrese el arreglo de personas");
        return personas;
    }


    public int update(Connection conn, Persona persona, String pk) throws SQLException {
        String sql = "update persona set nombre = ?, edad = ?, genero = ? , estatus = ? where nombre = " + "'"  + pk + "'";
        PreparedStatement pstmt = conn.prepareStatement(sql);

        pstmt.setString(1, persona.getNombre());
        pstmt.setInt(2, persona.getEdad());
        pstmt.setString(3, persona.getGenero());
        pstmt.setString(4, persona.getEstatus());

        int i = pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Ya se ha actualizado el registro");
        return i;
    }


    public int delete(Connection conn, String pk) throws SQLException {
        String sql = "delete from persona where nombre = " +"'"  + pk + "'";
        Statement stmt;
        stmt = conn.createStatement();
        int i = stmt.executeUpdate(sql);
        stmt.close();
        System.out.println("Se ha eliminado el registro");
        return i;
    }


}

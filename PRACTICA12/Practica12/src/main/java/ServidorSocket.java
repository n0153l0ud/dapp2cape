import conexion.Conexion;
import crud.CRUD;
import modelo.Persona;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;

public class ServidorSocket {
    public static void main(String[] args) {
        int puerto=8000;

        ArrayList<Persona> misPersonas= new ArrayList<>() ;
        try {

            ServerSocket miServidor = new ServerSocket(puerto);
            Socket miCliente;

            System.out.println("Esperando Conexion...");

            miCliente= miServidor.accept();

            System.out.println("Recibi Conexion");


            //----- Habilitar lectura y escritura del Socket
            BufferedReader lectorSocket;
            PrintWriter escritorSocket;

            InputStreamReader entradaDatos = new InputStreamReader(miCliente.getInputStream());
            lectorSocket= new BufferedReader(entradaDatos );

            escritorSocket= new PrintWriter(miCliente.getOutputStream(),true);
            //----


            String mensajeRespuesta, mensajeRecibido="",lineaRecibida;
            int opcion=0, totalContenido=0;

            System.out.println("Esperando mensaje....");

            lineaRecibida= lectorSocket.readLine();
            mensajeRecibido=lineaRecibida+"\n";


            if (lineaRecibida.contains("GET"))
            {
                opcion=1;
            }
            else if (lineaRecibida.contains("POST"))
            {
                opcion=2;
            }
            else if (lineaRecibida.contains("PUT"))
            {
                opcion=3;
            }
            else if (lineaRecibida.contains("DELETE"))
            {
                opcion=4;
            }
            else
            {
                opcion=-1;
            }

            System.out.println("Opcion: " + opcion);
            System.out.println(mensajeRecibido);
            if (opcion==1)
            {
                Conexion conexion = new Conexion();
                Connection conn = conexion.conecta();
                CRUD crud = new CRUD();

                misPersonas = crud.select(conn);
                String contenido= Persona.toXmlList(misPersonas);
                System.out.println(contenido);

                escritorSocket.println(contenido);

                conexion.desconecta(conn);

            }
            else if (opcion==2)
            {
                String mensaje = "Llena los datos:";
                escritorSocket.println(mensaje);
                String mensajeRec;

                mensajeRec = lectorSocket.readLine();
                System.out.println(mensajeRec);
                Persona persona = new Persona();

                persona.fromXml(mensajeRec);

                Conexion conexion = new Conexion();
                Connection conn = conexion.conecta();
                CRUD crud = new CRUD();

                crud.insert(conn,persona);

                conexion.desconecta(conn);
                mensajeRespuesta = "Datos insertados correctamente";
                escritorSocket.println(mensajeRespuesta);
            }
            else if (opcion == 3)
            {
                String clave;
                String confirmacion;
                String mensaje = "Ingresa el nombre de la persona a actualizar los datos:";
                escritorSocket.println(mensaje);

                clave = lectorSocket.readLine();

                System.out.println(clave);
                String mensaje3;

                mensaje3 = lectorSocket.readLine();

                System.out.println(mensaje3);

                Persona personaNueva = new Persona();

                personaNueva.fromJson(mensaje3);

                Conexion conexion = new Conexion();
                Connection conn = conexion.conecta();
                CRUD crud = new CRUD();

                crud.update(conn,personaNueva,clave);
                conexion.desconecta(conn);
                confirmacion = "Datos actualizados correctamente";
                escritorSocket.println(confirmacion);
            }
            else if(opcion == 4)
            {
                String clave;
                clave = lectorSocket.readLine();

                System.out.println(clave);

                Conexion conexion = new Conexion();
                Connection conn = conexion.conecta();
                CRUD crud = new CRUD();

                crud.delete(conn,clave);
                conexion.desconecta(conn);
                String mensaje = "Registro eliminado correctamente";
                escritorSocket.println(mensaje);
            }
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
        System.out.println("FIN del Programa");
    }
}

import modelo.Persona;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketCliente {
    public static void main(String[] args) {
        String opcion;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Socket miCliente = null;
        String host = "localhost";
        int puerto = 8000;
        try {
            System.out.println("Conectando al servidor");

            //Conexion al servidor mediante Socket TCP
            miCliente = new Socket(host, puerto);
            System.out.println("Me conecte al servidor");

            InputStreamReader streamSocket = new InputStreamReader(miCliente.getInputStream());

            BufferedReader lectorSocket = new BufferedReader(streamSocket);

            PrintWriter escritorSocket = new PrintWriter(miCliente.getOutputStream(),true);

            String mensajeTransmitido, mensajeRecibido;

            System.out.println("Ingresa la opcion a realizar: ");
            System.out.println("1.- GET");
            System.out.println("2.- POST");
            System.out.println("3.- PUT");
            System.out.println("4.- DELETE");
            opcion = br.readLine();
            mensajeTransmitido = opcion;
            escritorSocket.println(mensajeTransmitido);

            if(opcion.equals("GET")){
                System.out.println("Enviando mensaje");
                System.out.println("Recibiendo mensaje");
                mensajeRecibido = lectorSocket.readLine();

                System.out.println("La lista de personas es:");
                System.out.println(mensajeRecibido);
            }
            else if(opcion.equals("POST"))
            {
                System.out.println("Enviando mensaje");
                mensajeRecibido = lectorSocket.readLine();
                System.out.println(mensajeRecibido);
                String nombre, genero, estatus;
                Integer edad;
                Persona persona = new Persona();
                System.out.println("Ingresa el nombre: ");
                nombre = br.readLine();

                System.out.println("Ingresa la edad: ");
                edad = Integer.parseInt(br.readLine());

                System.out.println("Ingresa el genero: ");
                genero = br.readLine();

                System.out.println("Ingresa el estatus");
                estatus = br.readLine();

                System.out.println("Recibiendo mensaje");

                persona.setNombre(nombre);
                persona.setEdad(edad);
                persona.setGenero(genero);
                persona.setEstatus(estatus);

                mensajeTransmitido = persona.toXml();

                escritorSocket.println(mensajeTransmitido);

                mensajeRecibido = lectorSocket.readLine();

                System.out.println(mensajeRecibido);
            }
            else if (opcion.equals("PUT"))
            {
                String nombre;
                mensajeRecibido = lectorSocket.readLine();
                System.out.println(mensajeRecibido);
                nombre = br.readLine();

                mensajeTransmitido = nombre;
                escritorSocket.println(mensajeTransmitido);
                String nombreNuevo, generoNuevo, estatusNuevo;
                Integer edadNueva;

                Persona personaNueva = new Persona();
                System.out.println("Ingresa el nuevo nombre: ");
                nombreNuevo = br.readLine();

                System.out.println("Ingresa la nueva edad: ");
                edadNueva = Integer.parseInt(br.readLine());

                System.out.println("Ingresa el nuevo genero: ");
                generoNuevo = br.readLine();

                System.out.println("Ingresa el nuevo estatus");
                estatusNuevo = br.readLine();

                personaNueva.setNombre(nombreNuevo);
                personaNueva.setEdad(edadNueva);
                personaNueva.setGenero(generoNuevo);
                personaNueva.setEstatus(estatusNuevo);

                mensajeTransmitido = personaNueva.toJson();

                escritorSocket.println(mensajeTransmitido);

                mensajeRecibido = lectorSocket.readLine();

                System.out.println(mensajeRecibido);
            }
            else if ((opcion.equals("DELETE")))
            {
                String nombre;
                System.out.println("Ingresa el nombre de la persona a eliminar:");
                nombre = br.readLine();
                 mensajeTransmitido = nombre;

                 escritorSocket.println(mensajeTransmitido);
                 mensajeRecibido = lectorSocket.readLine();
                System.out.println(mensajeRecibido);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

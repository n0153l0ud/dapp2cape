-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: ibero_viajes2
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aeropuerto`
--

DROP TABLE IF EXISTS `aeropuerto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aeropuerto` (
  `id_aeropuerto` int NOT NULL AUTO_INCREMENT,
  `nombre_aeropuerto` varchar(45) NOT NULL,
  `cantidad_puertas_embarque` int NOT NULL,
  `cantidad_salas_de_espera` int NOT NULL,
  `id_ciudad_fk` int NOT NULL,
  PRIMARY KEY (`id_aeropuerto`),
  KEY `fk_ciudad_idx` (`id_ciudad_fk`),
  CONSTRAINT `fk_ciudad` FOREIGN KEY (`id_ciudad_fk`) REFERENCES `ciudad` (`id_ciudad`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aeropuerto`
--

LOCK TABLES `aeropuerto` WRITE;
/*!40000 ALTER TABLE `aeropuerto` DISABLE KEYS */;
INSERT INTO `aeropuerto` VALUES (1,'Bilbo Bolsón',1,1,1),(2,'Altos Elfos',3,3,2),(3,'Caballo Verde',1,1,3),(4,'Elfos Silvanos',1,1,4),(5,'Durin',1,1,5),(6,'Isildur',3,3,6),(7,'Khazad Dum',1,1,7),(8,'Mago Blanco',1,1,8),(9,'Ojo de Sauron',3,3,9),(10,'León Real',3,3,10),(11,'Príncipe Caspian',3,3,11),(12,'Bruja Blanca',3,3,12),(13,'Mago de Oz',3,3,13),(14,'Bruja del Oeste',3,3,14),(15,'Dorita',1,1,15);
/*!40000 ALTER TABLE `aeropuerto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avion`
--

DROP TABLE IF EXISTS `avion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `avion` (
  `id_avion` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id_avion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avion`
--

LOCK TABLES `avion` WRITE;
/*!40000 ALTER TABLE `avion` DISABLE KEYS */;
INSERT INTO `avion` VALUES (1,'Avion1'),(2,'Avion2'),(3,'Avion3'),(4,'Avion4');
/*!40000 ALTER TABLE `avion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boleto`
--

DROP TABLE IF EXISTS `boleto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `boleto` (
  `id_boleto` int NOT NULL AUTO_INCREMENT,
  `numero_asiento` varchar(2) NOT NULL,
  `id_vuelo` int NOT NULL,
  PRIMARY KEY (`id_boleto`),
  KEY `fk_id_vuelo_idx` (`id_vuelo`),
  CONSTRAINT `fk_id_vuelo` FOREIGN KEY (`id_vuelo`) REFERENCES `vuelo` (`id_vuelo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boleto`
--

LOCK TABLES `boleto` WRITE;
/*!40000 ALTER TABLE `boleto` DISABLE KEYS */;
INSERT INTO `boleto` VALUES (4,'A0',3),(5,'A1',3),(6,'A2',3),(7,'A0',4),(8,'A1',4);
/*!40000 ALTER TABLE `boleto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ciudad` (
  `id_ciudad` int NOT NULL AUTO_INCREMENT,
  `ciudad` varchar(45) NOT NULL,
  `id_pais_fk` int NOT NULL,
  PRIMARY KEY (`id_ciudad`),
  KEY `fk_pais_idx` (`id_pais_fk`),
  CONSTRAINT `fk_pais` FOREIGN KEY (`id_pais_fk`) REFERENCES `pais` (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` VALUES (1,'La Comarca',1),(2,'Rivendel',1),(3,'Rohan',1),(4,'Reino del Bosque',1),(5,'Erebor',1),(6,'Gondor',1),(7,'Moria',1),(8,'Isengard',1),(9,'Mordor',1),(10,'Narnia',2),(11,'Telmar',2),(12,'Charn',2),(13,'Ciudad Esmeralda',3),(14,'Winkie',3),(15,'Munchkin',3);
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nacionalidad`
--

DROP TABLE IF EXISTS `nacionalidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nacionalidad` (
  `id_nacionalidad` int NOT NULL AUTO_INCREMENT,
  `nacionalidad` varchar(45) NOT NULL,
  PRIMARY KEY (`id_nacionalidad`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nacionalidad`
--

LOCK TABLES `nacionalidad` WRITE;
/*!40000 ALTER TABLE `nacionalidad` DISABLE KEYS */;
INSERT INTO `nacionalidad` VALUES (1,'mediaterrano'),(2,'narniano'),(3,'ozniano');
/*!40000 ALTER TABLE `nacionalidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago`
--

DROP TABLE IF EXISTS `pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pago` (
  `id_pago` int NOT NULL AUTO_INCREMENT,
  `numero_tarjeta` varchar(16) NOT NULL,
  `monto` double NOT NULL,
  PRIMARY KEY (`id_pago`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago`
--

LOCK TABLES `pago` WRITE;
/*!40000 ALTER TABLE `pago` DISABLE KEYS */;
INSERT INTO `pago` VALUES (5,'1216444477778888',23775),(6,'9999555511110000',10650);
/*!40000 ALTER TABLE `pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pais` (
  `id_pais` int NOT NULL AUTO_INCREMENT,
  `pais` varchar(45) NOT NULL,
  PRIMARY KEY (`id_pais`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'Tierra Media'),(2,'Narnia'),(3,'Oz');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raza`
--

DROP TABLE IF EXISTS `raza`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `raza` (
  `id_raza` int NOT NULL AUTO_INCREMENT,
  `raza` varchar(45) NOT NULL,
  PRIMARY KEY (`id_raza`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raza`
--

LOCK TABLES `raza` WRITE;
/*!40000 ALTER TABLE `raza` DISABLE KEYS */;
INSERT INTO `raza` VALUES (1,'humano'),(2,'elfo'),(3,'enano'),(4,'orco'),(5,'duende'),(6,'mago'),(7,'bruja'),(8,'centauro'),(9,'fauno'),(10,'mono alado');
/*!40000 ALTER TABLE `raza` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `id_rol` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `id_usuario_fk` int NOT NULL,
  PRIMARY KEY (`id_rol`),
  KEY `id_usuario_fk_idx` (`id_usuario_fk`),
  CONSTRAINT `id_usuario_fk` FOREIGN KEY (`id_usuario_fk`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (2,'ADMIN',1),(3,'USER',2),(4,'USER',5),(5,'USER',4),(6,'USER',3),(7,'USER',6),(8,'USER',7);
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `id_usuario` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `a_paterno` varchar(45) NOT NULL,
  `a_materno` varchar(45) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `nacionalidad` int NOT NULL,
  `raza` int NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(256) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `fk_nacionalidad_idx` (`nacionalidad`),
  KEY `fk_raza_idx` (`raza`),
  CONSTRAINT `fk_nacionalidad` FOREIGN KEY (`nacionalidad`) REFERENCES `nacionalidad` (`id_nacionalidad`),
  CONSTRAINT `fk_raza` FOREIGN KEY (`raza`) REFERENCES `raza` (`id_raza`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Omar','Vazquez','Solis','1980-01-01',2,1,'55555','omar@ibero.mx','$2a$10$hBzdKkcgLPAE1XovJz7Dr.I1.9NB0yphVLNfU99rJHYe2oMlrd9LC'),(2,'Emilio','Caballero','Rojas','1999-02-02',2,1,'55551','emilio@ibero.mx','$2y$12$2UyA21nuKUYt3GtIQhuFnOsfzqDvO5rZndJwwYCQUb8103VWIIkcW'),(3,'Pedro','Picapiedra','Marmol','1990-05-06',1,7,'333333','pedro@ibero.mx','$2a$10$J84aQr1yJvlWXinuGScCQOZHWFkDVHnFbU2QHyLmDv5H3VxlRUvni'),(4,'El pepe','El pepe','El pepe','2021-05-14',1,4,'555','pepe@ibero.mx','$2a$10$G7rg5nju17s5oW32m6dJg.4rRbhqOdCCPHAUEK8rM6r9OYtMv2yXi'),(5,'La Chona','Se','Mueve','2021-05-07',2,7,'555','chona@ibero.mx','$2a$10$rqPWemEUkdh6r21ZAXosDuLVgoLRbH4C9JXTkvrRxWt9EieqVjO6e'),(6,'Usuario1','Paterno1','Materno1','2021-05-04',2,7,'5552675273','usuario1@gmail.com','$2a$10$SApqS1ZhQum3/8sz.TYgFOskWC0uIiA1jnp0hjQVj7jDVk.YhuP1a'),(7,'Nombre01','Paterno01','Materno01','2021-05-04',2,8,'1122335500','nombre1@gmail.com','$2a$10$Y6yL/yh/3SCECgYHaGzH7.FnVuW0BHfPi42lODLI71xASniVLv58C');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `viaje`
--

DROP TABLE IF EXISTS `viaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `viaje` (
  `id_viaje` int NOT NULL AUTO_INCREMENT,
  `origen` varchar(45) NOT NULL,
  `destino` varchar(45) NOT NULL,
  `fecha` date NOT NULL,
  `ruta` varchar(255) NOT NULL,
  PRIMARY KEY (`id_viaje`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `viaje`
--

LOCK TABLES `viaje` WRITE;
/*!40000 ALTER TABLE `viaje` DISABLE KEYS */;
INSERT INTO `viaje` VALUES (5,'Moria','Telmar','2021-05-04','[Moria 0.0, Isengard 1300.0, Rohan 2600.0, Gondor 4150.0, Narnia 6125.0, Telmar 7925.0]'),(6,'Rohan','Telmar','2021-05-10','[Rohan 0.0, Gondor 1550.0, Narnia 3525.0, Telmar 5325.0]');
/*!40000 ALTER TABLE `viaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vuelo`
--

DROP TABLE IF EXISTS `vuelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vuelo` (
  `id_vuelo` int NOT NULL AUTO_INCREMENT,
  `tipo_vuelo` varchar(45) NOT NULL,
  `hora` varchar(10) NOT NULL,
  `id_aeropuerto_origen` int NOT NULL,
  `destino` varchar(45) NOT NULL,
  `id_avion_fk` int NOT NULL,
  `id_viaje_fk` int NOT NULL,
  `id_usuario_fk` int NOT NULL,
  `id_pago_fk` int NOT NULL,
  PRIMARY KEY (`id_vuelo`),
  KEY `fk_viaje_idx` (`id_viaje_fk`),
  KEY `fk_avion_idx` (`id_avion_fk`),
  KEY `fk_usuario_idx` (`id_usuario_fk`),
  KEY `fk_pago_idx` (`id_pago_fk`),
  KEY `fk_aeropuerto_idx` (`id_aeropuerto_origen`),
  CONSTRAINT `fk_aeropuerto` FOREIGN KEY (`id_aeropuerto_origen`) REFERENCES `aeropuerto` (`id_aeropuerto`),
  CONSTRAINT `fk_avion` FOREIGN KEY (`id_avion_fk`) REFERENCES `avion` (`id_avion`),
  CONSTRAINT `fk_pago` FOREIGN KEY (`id_pago_fk`) REFERENCES `pago` (`id_pago`),
  CONSTRAINT `fk_usuario` FOREIGN KEY (`id_usuario_fk`) REFERENCES `usuario` (`id_usuario`),
  CONSTRAINT `fk_viaje` FOREIGN KEY (`id_viaje_fk`) REFERENCES `viaje` (`id_viaje`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vuelo`
--

LOCK TABLES `vuelo` WRITE;
/*!40000 ALTER TABLE `vuelo` DISABLE KEYS */;
INSERT INTO `vuelo` VALUES (3,'Nacional','07:00',7,'Telmar',3,5,7,5),(4,'Nacional','05:00',3,'Telmar',4,6,7,6);
/*!40000 ALTER TABLE `vuelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'ibero_viajes2'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-04 23:56:54

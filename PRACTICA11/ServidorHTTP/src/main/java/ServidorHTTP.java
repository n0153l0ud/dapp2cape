import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ServidorHTTP {
    public static void main(String[] args) {

        int puerto = 8000;
        try {
            String pagina = "";
            ServerSocket miServidor = new ServerSocket(puerto);
            Socket miCliente = null;

            System.out.println("Esperando conexion");

            miCliente = miServidor.accept();

            System.out.println("Conexion aceptada");

            InputStreamReader streamSocket = new InputStreamReader(miCliente.getInputStream());

            BufferedReader lectorSocket = new BufferedReader(streamSocket);

            PrintWriter escritorSocket = new PrintWriter(miCliente.getOutputStream(), true);

            String mensajeRecibido;
            StringBuilder mensajeEnviado = new StringBuilder();

            //while (true){
            System.out.println("Esperando mensaje");

            mensajeRecibido = lectorSocket.readLine();

            System.out.println(mensajeRecibido);

            File fichero = new File("E:\\ServidorHTTP\\pagina.html");
            Scanner s = null;
            StringBuilder contenido = new StringBuilder();
            try {
                // Leemos el contenido del fichero
                System.out.println("... Leemos el contenido del fichero ...");
                s = new Scanner(fichero);
                // Leemos linea a linea el fichero
                while (s.hasNextLine()) {
                    contenido.append(s.nextLine());    // Guardamos la linea en un String
                }
            } catch (Exception ex) {
                System.out.println("Mensaje: " + ex.getMessage());
            } finally {
                // Cerramos el fichero tanto si la lectura ha sido correcta o no
                try {
                    mensajeEnviado.append( "HTTP/1.1 200 OK\n");

                    mensajeEnviado.append("Content-Type: text/html\n");

                    mensajeEnviado.append( "Content-Lenght: " + pagina.length() + "\n\n");

                    mensajeEnviado.append(contenido.toString());

                    System.out.println("Mensaje a enviar: ");
                    System.out.println(mensajeEnviado);

                    escritorSocket.println(mensajeEnviado);

                    System.out.println("Enviando mensaje");
                    if (s != null)
                        s.close();
                } catch (Exception ex2) {
                    System.out.println("Mensaje 2: " + ex2.getMessage());
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
